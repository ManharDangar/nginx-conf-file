server {
	listen 80;
	listen [::]:80;

	server_name agrisathi.com;


	location / {
	    proxy_pass http://localhost:5000;
	    proxy_set_header Host $http_host;
      	    proxy_set_header X-Real-IP $remote_addr;
	    proxy_set_header Accept-Encoding "";	  
	}
}
